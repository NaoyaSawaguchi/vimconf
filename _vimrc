source $VIMRUNTIME/vimrc_example.vim " デフォルトの設定ファイルを読み込む
scriptencoding utf-8                 " Vimスクリプトファイルの文字コードをしてい

"表示関係
set number                   " 行番号の表示
set list                     " 不可視文字を表示
set showmatch                " 対応するカッコを表示
set ruler                    " カーソル位置の表示
set showcmd                  " 入力中のコマンドを表示
set listchars=trail:_,tab:>-,extends:>,nbsp:% " 不可視文字の表示方法を指定
"Tab文字の色設定
augroup set-tab-highlight
  autocmd!
  autocmd ColorScheme,GUIEnter * hi SpecialKey guibg=NONE guifg=gray40
augroup END

" 全角スペースの表示
augroup highlightIdegraphicSpace
  autocmd!
  autocmd Colorscheme * highlight IdeographicSpace term=underline ctermbg=DarkGreen guibg=DarkGreen
  autocmd VimEnter,WinEnter * match IdeographicSpace /　/
augroup END
"
"ファイルの処理
set hidden   " 保存しなくても別ファイルを開ける
set autoread " 外部でファイルに変更がされた場合は読みなおす
set confirm  " 保存されていないファイルがあるときは終了前に保存確認

"検索・置換
set gdefault   " 置換じのgオプションをデフォで有効
set incsearch  " インクリメンタルサーチを行う
set smartcase  " 大文字と小文字が混じったら、大文字小文字を区別する
set wrapscan   " 最後まで行ったら最初に戻る
set hlsearch   " 検索結果をハイライトする

"タブやインデントの設定
set cindent      " C言語風のインデントを自動で行う
set autoindent   " 自動でインデントを行う
set smartindent  " 改行時に自動でインデント
set expandtab    " Tabを半角スペースに変換する
set tabstop=2    " 画面上でタブ文字が占める幅
set shiftwidth=2 " Vimが自動で生成するTag幅

"動作環境の設定
set iminsert=2                       " インサートモードから抜けると自動的にIMEをオフにする
set iminsert=0                       " 起動時いきなり半角入力
set imsearch=-1                      " 起動時いきなり半角入力
set fileencoding=utf-8               " 保存するときはUTF-8
set fileencodings=utf-8,sjis         " 読み込むときのファイルの優先順位
set directory=~/.vim/tmp/swap        " スワップファイルのディレクトリを指定
set backupdir=~/.vim/tmp/backup      " バックアップファイルのディレクトリを指定
set undodir=~/.vim/tmp/undo          " undoファイルのディレクトリを指定
"source $VIMRUNTIME/mswin.vim        " mswin.vimを読み込む
"behave mswin                        " mswin.vimを読み込む
"set nocompatible                    " Vi互換モードで起動する

" NeoBundleの設定
set runtimepath+=~/.vim/bundle/neobundle.vim/  " bundleで管理するディレクトリを指定
call neobundle#begin(expand('~/.vim/bundle/')) " Required:
NeoBundleFetch 'Shougo/neobundle.vim'          " neobundle自体をneobundleで管理

" 使用するプラグインの一覧
NeoBundle 'mattn/emmet-vim'                    " HTML/CSSの記述を支援する
NeoBundle 'hail2u/vim-css3-syntax'             " CSS3のシンタックスを提供
"NeoBundle 'taichouchou2/html5.vim'             " html5のタグ定義
NeoBundle 'tpope/vim-surround'                 " 文字列を記号で囲む系の編集を効率化
NeoBundle 'Shougo/neocomplcache'               " ?
NeoBundle 'Shougo/neosnippet'                  " ?
NeoBundle 'Shougo/neosnippet-snippets'         " ?
NeoBundle 'h1mesuke/vim-alignta'               " 記号を指定し、その位置を各行で揃える。
"NeoBundle 'stephpy/vim-yaml'                   " YAMLのシンタックスハイライト
NeoBundle 'chase/vim-ansible-yaml'              " Ansibleのyaml
call neobundle#end()

" ------------------------------
" 自分定義のキーマップ
"-------------------------------
imap <C-f> <C-x><C-o>
noremap <S-h> ^
noremap <S-l> $
noremap m %
nnoremap <C-h> :noh<CR>
vnoremap <S-a> :Alignta 
"検索をする際、デフォルトでvery magicモードにする
nnoremap / /\v

" ------------------------------
" 言語ごとのインデント設定
"-------------------------------
augroup phpConfig
 autocmd!
 autocmd BufNewFile,BufRead *.php setlocal tabstop=2
 autocmd BufNewFile,BufRead *.php setlocal shiftwidth=1
 autocmd BufNewFile,BufRead *.php setlocal noexpandtab
augroup END "of phpConfig

augroup yamlConfig
  autocmd!
  " .ymlファイルを読み込んだ時はファイルタイプをansibleにする。
  " chase/vim-ansible-yamlプラグインを利用
  autocmd BufNewFile,BufRead *.yml setlocal filetype=ansible
augroup END "of yamlConfig


""""" erlang用設定
" erlファイルをerlangとして認識する
au BufNewFile,BufRead *.erl setf erlang
" :makeでerlang構文チェック
au FileType erlang setlocal makeprg=erlc\ %
au FileType erlang setlocal errorformat=%f:%l:\ %m

" Required:
filetype plugin indent on
NeoBundleCheck "未インストールのプラグインがあればインストールを促す

" -------------------------------------
" 以下、プラグインで必要な記述のコピペ
"       使わなくなったら消そう
" -------------------------------------

" neocomplcache の設定
let g:acp_enableAtStartup = 0             " Disable AutoComplPop.
let g:neocomplcache_enable_at_startup = 1 " Use neocomplcache.
let g:neocomplcache_enable_smart_case = 1 " Use smartcase.
let g:neocomplcache_min_syntax_length = 3 " Set minimum syntax keyword length.
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'
" Define dictionary.
let g:neocomplcache_dictionary_filetype_lists = {
    \ 'default' : ''
    \ }
" Plugin key-mappings.
inoremap <expr><C-g>     neocomplcache#undo_completion()
inoremap <expr><C-l>     neocomplcache#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplcache#smart_close_popup() . "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplcache#smart_close_popup()."\<C-h>"
"inoremap <expr><C-y>  neocomplcache#close_popup()
inoremap <expr><C-e>  neocomplcache#cancel_popup()

" neosnippetの設定
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif
